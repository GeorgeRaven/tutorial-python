FROM archlinux:latest

# directory containing source files with respect to dockerfile
ARG SRCDIR="."
# directory where the source will exist in the dockerfile
ARG PKGDIR="/app"

# resetting all gnupg keys
RUN rm -rf /etc/pacman.d/gnupg && \
    pacman-key --init && \
    pacman-key --populate archlinux

# resync all packages and check keyring
RUN pacman -Syyuu \
    archlinux-keyring \
    base-devel \
    --noconfirm

# install other packages desirable for our python install
RUN pacman -S \
    base-devel \
    cmake \
    clang \
    git \
    python \
    python-pip \
    python-tqdm \
    python-numpy \
    python-pandas \
    python-matplotlib \
    python-seaborn \
    python-configargparse \
    python-sphinx \
    python-sphinx-argparse \
    python-sphinx_rtd_theme \
    python-ipykernel \
    jupyterlab \
    --noconfirm

# making the directory we will store all our source code in
RUN mkdir -p ${PKGDIR}

# set new directory as our default directory
WORKDIR ${PKGDIR}

# copy our requirements.txt to the container
COPY ${SRCDIR}/requirements.txt ${PKGDIR}/requirements.txt

# install additional dependencies listed in requirements.txt
RUN pip3 install -r ${PKGDIR}/requirements.txt

# copy in the rest of our files seperateley after installing dependencies
# so we dont keep reinstalling dependencies when changing any other file
COPY ${SRCDIR} ${PKGDIR}

# launch jupyter lab in the current default directory
ENTRYPOINT jupyter lab --allow-root --no-browser --ip "*"

# display some statistics about the container
RUN printf "\033[0;31m%s%s\n\033[0m" "Total Packages: " $(pacman -Q | wc -l) && \
    pacman -Qi | gawk '/^Name/ { x = $3 }; /^Installed Size/ { sub(/Installed Size  *:/, ""); print x":" $0 }' | sort -k2,3rn | head -n10 | gawk 'BEGIN { print "\n""These are your 10 largest programs:""\n" }; { print }'
