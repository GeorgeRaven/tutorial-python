#!/usr/bin/env bash
docker build -t archer/jupyter -f Dockerfile . # --no-cache
docker run -v "${PWD}/python-basics":"/app/python-basics" -v "${PWD}/matplotlib":"/app/matplotlib" -p 127.0.0.1:8888:8888 -it archer/jupyter
