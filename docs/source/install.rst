.. include:: substitutions

Install
=======

There are two ways to interact with these tutorials:

- Docker; easily reproducible, can get going very quickly, more-secure and works across all platforms nearly the same.
- Raw python install + jupyter lab; native to any given system and less layers of abstraction.

Docker
------

.. warning::

  Windows users may prefer to manually install python instead of using docker, purely because it seems docker is slightly awkward on windows, and may involve more in depth work than it may be worth!

To use these tutorials with |docker|_ you must have docker installed and a browser thats about it for dependencies.

.. note::

  For Linux users, im sure you know where to look.
  For Windows users go to: https://docs.docker.com/docker-for-windows/install/ please go through the steps one by one, and be aware this will likely require a reboot. For both windows and Linux users, make sure the docker engine is running, E.g for windows users start the docker app once you have installed it all successfully.

To run the |docker|_ container once everything is installed you will need to run the following in a terminal (like `powershell <https://docs.microsoft.com/en-us/powershell/scripting/overview?view=powershell-7.1>`_ on windows, or bash on linux) with access to |docker|_:

.. code:: bash

  docker run -v "${PWD}/python-basics":"/app/python-basics" -v "${PWD}/matplotlib":"/app/matplotlib" -p 127.0.0.1:8888:8888 -it registry.gitlab.com/georgeraven/tutorial-python:master

.. note::

  if you are on windows these paths will need to be backslashes instead of forward slashes but not the one that says "registry.gitlab.com/georgeraven/tutorial-python:master" that is always correct no matter the platform as its a url. If this is too problematic you can instead run:

.. code:: bash

  docker run -p 127.0.0.1:8888:8888 -it registry.gitlab.com/georgeraven/tutorial-python:master

but this latter command will mean your work will be ephemeral/ wont live longer than docker is running, but is great for a little playground to work in and is still a great way to start learning.

Once this is running you should get a URL (the last of the 3) that you can paste into your browser and it will load you in to an environment with everything pre-prepared for you!

Build the Container Yourself
++++++++++++++++++++++++++++

If for whatever reason you cant connect to gitlab to get the ready made image, or lets say you are on an ARM machine then you may need to build the docker container locally.
I have included a little script to build and run the docker container for you, called "docker-run-jupyter.sh" (Linux bash script)! However as far as I am aware archlinux does not by default work on ARM so there may need to be some tweaking.

Raw Python Install
------------------

So this method is heavily not preferred as it can be quite involved. But you will need to install (follow the links for more in depth instructions):

* `python3 <https://www.python.org/downloads/windows/>`_
* `python3-pip <https://docs.python.org/3/library/ensurepip.html>`_ (if it does not come already with python which it will in most cases)
* `jupyter lab <https://jupyter.org/install>`_
* git (or github `desktop <https://desktop.github.com/>`_ on Windows)
* `powershell <https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-windows?view=powershell-7.1>`_ (only for windows, as functions as your much more capable terminal)

on Linux this is easy but on windows you will have to use your browser and navigate to the respective pages to download the installers (follow the links).

You from a terminal (like powershell) will need to download the tutorial files:

.. code:: bash

  git clone https://gitlab.com/GeorgeRaven/tutorial-python

Then you will need to change directory into the newly downloaded folder to continue

.. code:: bash

  cd tutorial-python

Now you can install any remaining dependencies contained in "requirements.txt" with one command thanks to pip:

.. code:: bash

  pip install -r requirements.txt

You can then run the command:

.. code:: bash

  jupyter lab

Which will give you the same link as above which you can paste into your web browser to continue and that is it for installation you should be able to navigate to the tutorials of interest using the graphical web interface now.
