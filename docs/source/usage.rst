Usage
=====

.. |jupyter-fig| image:: img/lab.png
  :width: 700
  :alt: Image of Jupyter Lab launch page.

Now that jupyter lab is running either through docker or through a local install of jupyter lab. We can access these tutorials by taking a look at the output in our terminal of jupyter lab, you should see something like this towards the end:

.. code::

      To access the server, open this file in a browser:
        file:///root/.local/share/jupyter/runtime/jpserver-1-open.html
    Or copy and paste one of these URLs:
        http://18eb966b0e40:8888/lab?token=4495d3295b496137bd8f9e3286b17fe2ee719adc6f82e66e
     or http://127.0.0.1:8888/lab?token=4495d3295b496137bd8f9e3286b17fe2ee719adc6f82e66e

If you are using docker then you will need to look in your own terminal and use the bottom link. If you are using a local installation of jupyter lab you can use any of the three links.

This will take you to jupyter lab and automatically log you in, since your token is included in your link which is why you need to use your own link not the example I have included above.

You will see something akin to the following:

|jupyter-fig|

You can browse the files on the left hand side, and go to the relevant tutorial for you, e.g python-basics contains files for learning the basics of python!

Open the notebook like python-basics.ipynb by double clicking and this will launch the tutorial for you. You can interact with the tutorial by running "cells" which are little blocks of code, and you can insert and run your own. The tutorials should talk you through further. Enjoy and good luck!
